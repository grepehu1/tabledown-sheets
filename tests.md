# Benchmark Serverless Services 

A few tests after comparing serveless services:

| Service | Bundle Size (The Entire App Compiled) | Is Open Source? | Observation |
|---|---|---|---|
| AppWrite | 497 kb | Yes | Had to use JSON fields stringified, which is heavy on the client-side; Also response time was hitting 2.3 seconds, too slow.
| PocketBase | 507 kb (477 kb without SDK) | Yes | Only self-hosted, will be more expesive to start, at least $5 a month while others all start at free tiers. By far the fastest response time, API written in Golang |
| Supabase | 567 kb | Yes | Response time a little above 500 ms, kind of slow but acceptable, easy to workaround by distributing DB in the edge; By far the easiest to setup and run |
| Firebase (What I currently use) | 803 kb | No | Owned by Google; The second easiest to setup and run; The biggest free tier of them all; Reponse speed magnific even without DB on edge. |